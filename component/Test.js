import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Button, StyleSheet } from 'react-native';
import Regis from './register/Regis';
import firebase from 'firebase';
import Navigation from './Navigation';
import Firebase from './Firebase';

export default class Test extends Component {




    logOutButtonPass = () => {
        firebase.auth().signOut();
    }



    render() {
        return (<View >


            <Button
                title="ออกจากระบบ"
                onPass={() => firebase.auth().signOut()}
                
            ></Button>

            <Button
                title="เข้าสู่ระบบ"
                onPass={() => this.props.navigation.navigate(Firebase)}

            ></Button>

        </View>
        )
    }
}
