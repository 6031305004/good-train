import React from 'react';
import 'react-native-gesture-handler';
import { StyleSheet, Text, View, Card } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';


import App from '../App';
import Regis from './register/Regis';
import Login from './Login';
import Firebase from './Firebase';
import LoginForm from './LoginForm';
import Test from './Test';
import Loginpass from './Loginpass';
import firebase from 'firebase';


const Stack = createStackNavigator();




export default function Navigation() {
  return (
    <NavigationContainer >
      <Stack.Navigator >
    
      <Stack.Screen
          name="Loginpass"
          component={Loginpass}
          options={{ title: 'Pass' }}
        />
        <Stack.Screen
          name="Firebase"
          component={Firebase}
          options={{ title: 'Welcome' }}
        />
         <Stack.Screen
          name="Regis"
          component={Regis}
          options={{ title: 'Registration' }}
        />
        <Stack.Screen
          name="Login"
          component={Login}
        />
        <Stack.Screen
          name="LoginFrom"
          component={LoginForm}
          options={{ title: 'Login' }}
        />
        <Stack.Screen
          name="Test"
          component={Test}
          options={{ title: 'Test' }}
        />
        <Stack.Screen
          name="App"
          component={App}
          options={{ title: 'App' }}
        />
        
      </Stack.Navigator>
    </NavigationContainer>
  );
}



