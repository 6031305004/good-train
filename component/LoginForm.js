import React, { Component } from 'react';
import { StyleSheet, View, TextInput, TouchableOpacity, Text, StatusBar, Button } from 'react-native';
import firebase from 'firebase';
import Regis from './register/Regis';
import Test from './Test';



export default class LoginForm extends Component {

    state = {
        email: '',
        password: '',
        error: '',
        loading: false
    };

    onLoginButtonPress = () => {
        const { email, password } = this.state;

        this.setState({ loading: true });
        firebase.auth().signInWithEmailAndPassword(email, password)
            .then(() => {

                alert("Successful , " + email + " " + password);
            })
            .catch((msgError) => {

                alert(msgError.message);
            });

    }
    renderTouchableOpacity() {
        if (this.state.loading) {
            return (<ActivityIndicator size='small' />);
        } else {
            return (<TouchableOpacity style={styles.buttonContainer}
                onPress={this.onLoginButtonPress.bind(this)}>
                <Text style={styles.header}  >LOGIN</Text>
            </TouchableOpacity>)
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar
                    barStyle="light-content"
                />
                <TextInput
                    placeholder="Email"
                    placeholderTextColor="rgba(255,255,255,0.7)"
                    returnKeyType="next"
                    onSubmitEditing={() => this.passwordInput.focus()}
                    keyboardType="email-address"
                    autoCapitalize="none"
                    autoCorrect={false}
                    style={styles.input}
                    value={this.state.email}
                    onChangeText={str => this.setState({ email: str })}
                />
                <TextInput
                    placeholder="Password"
                    value={this.state.password}
                    placeholderTextColor="rgba(255,255,255,0.7)"
                    returnKeyType="go"
                    secureTextEntry
                    style={styles.input}
                    ref={(input) => this.passwordInput = input}
                    onChangeText={str => this.setState({ password: str })}
                />

                <Button
                    title="Login"
                    style={styles.buttom}
                    onPress={this.onLoginButtonPress.bind(this)}>
                </Button>

            </View>

        );
    }
}



const styles = StyleSheet.create({
    buttom: {
        alignSelf: 'stretch',
        alignItems: 'center',
        padding: 20,
        backgroundColor: '#59cbbd',
        marginTop: 30,
    },
    textinput: {
        alignSelf: 'stretch',
        height: 40,
        marginBottom: 30,
        textShadowColor: '#BEBEBE',
        color: '#fff',
        borderBottomColor: '#f8f8f8',
        borderBottomWidth: 1,
    },

    container: {
        padding: 20
    },
    input: {
        height: 40,
        width: 300,
        backgroundColor: 'rgba(255,255,255,0.2)',
        marginBottom: 10,
        color: '#FFF',
        paddingHorizontal: 10
    },
    buttonContainer: {
        backgroundColor: '#2980b9',
        paddingVertical: 15,
        marginBottom: 30,
    },
    buttonText: {
        textAlign: 'center',
        color: '#FFFFFF',
        fontWeight: '700',
    }
})