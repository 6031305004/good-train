import React, { Component } from 'react';
import { StyleSheet, View, Image, Text, KeyboardAvoidingView } from 'react-native';
import LoginForm from './LoginForm';

export default class Login extends Component {
    render() {
        return (
            <KeyboardAvoidingView behavior style={style.Component}>
                <View >
                    <Text style={style.header}>Login</Text>
                </View>
                <View style={StyleSheet.formContainer}>
                    <LoginForm />
                </View>
            </KeyboardAvoidingView>
        )
    }
}

const style = StyleSheet.create({
    header: {
        fontSize: 24,
        color: '#fff',
        paddingBottom: 10,
        marginTop: 60,
        marginBottom: 40,
        borderBottomColor: '#fff',
        borderBottomWidth: 3,
    },
    container: {
        flex: 1,
        backgroundColor: '#3498db'

    },
    title: {
        color: '#FFF',
        marginTop: 10,
        width: 160,
        textAlign: 'center',
        opacity: 0.9,
    }
})
