import React, { Component } from 'react'
import { AppRegistry } from 'react-native';
import { StyleSheet, Card, Button, View, Image, Text, KeyboardAvoidingView, Header,TouchableOpacity } from 'react-native';

import firebase from 'firebase';
import Firebase from './Firebase';
import Test from './Test';
import Regis from './register/Regis';
import Navigation from './Navigation';
import LoginForm from './LoginForm';








export default class Loginpass extends Component {

  state = { loggedIn: null };

  componentDidMount() {
    const firebaseConfig = {
      apiKey: "AIzaSyBF50RclZ-HbsvJHHTjY67DkVcjWlszNVw",
      authDomain: "train-me-food.firebaseapp.com",
      databaseURL: "https://train-me-food.firebaseio.com",
      projectId: "train-me-food",
      storageBucket: "train-me-food.appspot.com",
      messagingSenderId: "422912285665",
      appId: "1:422912285665:web:f77ad1b364c381466bd0a0",
      measurementId: "G-S39P11NKZR"
    };

    firebase.initializeApp(firebaseConfig);



    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.setState({ loggedIn: true });
      } else {
        this.setState({ loggedIn: false });
      }
    });
  }



  renderFormLogin() {
    if (this.state.loggedIn == true) {
      return (<View>

        <Text>Log IN แล้ว</Text>

        <Text>{firebase.auth().currentUser.email}</Text>
        <TouchableOpacity
                
                onPass={() => firebase.auth().signOut()}
                
            ><Text>ออกจากระบบ</Text>  
            </TouchableOpacity>
        
      </View>);

    } else {
      return (<View>
        <Text>ไม่ได้ Log In</Text>
        <Firebase />
        <Button title="ยังไม่มี Account ? สมัครสมาชิก"
                    style={style.buttom}
                    onPress={() => this.props.navigation.navigate(Regis)}
                />
      </View>
      );
    }
  }



  render() {
    return (<View style={style.backgot}>
      {this.renderFormLogin()}
    </View>);
  }
}

const style = StyleSheet.create({
  black: {
    flex: 1,
    backgroundColor: '#3498db',
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: 60,
    paddingRight: 60,
  },
  backgot: {
     flex : 1 ,
      backgroundColor: '#3490db',
      alignItems: 'center',
      justifyContent: 'center',
      paddingLeft: 60,
      paddingRight: 60,
  },
})


