import React, { Component } from 'react'
import { StyleSheet, View, TextInput, TouchableOpacity, Text, StatusBar, Button ,Alert} from 'react-native';

import Firebase from '../Firebase';
import firebase from 'firebase';



export default class Regis extends Component {


    state = {
        email: '',
        password: '',
        passwordConfirm : '',
        error: '',
        loading: false
    };

    onLoginButtonPress = () => {
        const { email, password, passwordConfirm } = this.state;
        if (this.state.password !== this.state.passwordConfirm) {
            Alert.alert("Passwords do not Match");
            return;
            
        }
            this.setState({ loading: true });
            firebase.auth().createUserWithEmailAndPassword(email, password)
                .then(() => {

                    alert("Successful , " + email + " " + password);
                })
                .catch(() => {

                    alert("Error");
                });

        }
        renderTouchableOpacity() {
            if (this.state.loading) {
                return (<ActivityIndicator size='small' />);
            } else {
                return (<TouchableOpacity style={styles.buttonContainer}
                    onPress={this.onLoginButtonPress.bind(this)}>
                    <Text style={styles.header}  >LOGIN</Text>
                </TouchableOpacity>)
            }
        }

        render() {
            return (
                <View style={styles.regis}>
                    <StatusBar
                        barStyle="light-content"
                    />
                    <Text style={styles.header}>สร้างบัญชีผู้ใช้</Text>
                    <TextInput style={styles.input} placeholder="ชื่อ"
                        placeholderTextColor="rgba(255,255,255,0.7)"
                        underlineColorAndroid={'transparent'}></TextInput>


                    <TextInput style={styles.input} placeholder="นามสกุล"
                        placeholderTextColor="rgba(255,255,255,0.7)"
                        underlineColorAndroid={'transparent'}></TextInput>

                    <TextInput
                        placeholder="Email"
                        placeholderTextColor="rgba(255,255,255,0.7)"
                        returnKeyType="next"
                        onSubmitEditing={() => this.passwordInput.focus()}
                        keyboardType="email-address"
                        autoCapitalize="none"
                        autoCorrect={false}
                        style={styles.input}
                        value={this.state.email}
                        onChangeText={str => this.setState({ email: str })}
                    />
                    <Text style={styles.textnm}>เช่น (xxx123@zzz.com)</Text>

                    <TextInput
                        placeholder="Password"
                        value={this.state.password}
                        placeholderTextColor="rgba(255,255,255,0.7)"
                        returnKeyType="go"
                        secureTextEntry
                        style={styles.input}
                        ref={(input) => this.passwordInput = input}
                        onChangeText={str => this.setState({ password: str })}
                    />
                    <Text style={styles.textnm}>ใช้อักขระและตัวเลข 6 ตัวขึ้นไป</Text>
                    <TextInput
                        placeholder="Confirm password"
                        value={this.state.passwordConfirm}
                        placeholderTextColor="rgba(255,255,255,0.7)"
                        returnKeyType="go"
                        secureTextEntry
                        style={styles.input}
                        ref={(input) => this.passwordInput = input}
                        onChangeText={str => this.setState({ passwordConfirm: str })}
                    />

<View style={{paddingTop: 50,  alignSelf: 'stretch', flexDirection: 'row', justifyContent: 'space-between' }}>
                    
                    <Button title="กลับไปหน้า Log in"
                        style={styles.buttom}
                        onPress={() => this.props.navigation.navigate(Firebase)}
                    />
                    <Button
                        title="สมัครสมาชิก"
                        style={styles.buttom}
                        onPress={this.onLoginButtonPress.bind(this)}
                        />
                    
                    </View>
                </View>

            );
        }
    }



    const styles = StyleSheet.create({
        buttom: {
            alignSelf: 'stretch',
            alignItems: 'center',
            padding: 20,
            backgroundColor: '#59cbbd',
            marginTop: 30,
        },
        textinput: {
            alignSelf: 'stretch',
            height: 40,
            marginBottom: 30,
            textShadowColor: '#BEBEBE',
            color: '#fff',
            borderBottomColor: '#f8f8f8',
            borderBottomWidth: 1,
        },

        container: {
            padding: 20
        },
        input: {
            height: 40,
            alignSelf: 'stretch',
            backgroundColor: 'rgba(255,255,255,0.2)',
            marginBottom: 10,

            color: '#FFF',
            paddingHorizontal: 10
        },
        buttonContainer: {
            backgroundColor: '#2980b9',
            paddingVertical: 15,
            marginBottom: 30,
        },
        buttonText: {
            textAlign: 'center',
            color: '#FFFFFF',
            fontWeight: '700',
        }, regis: {
            flex: 1,
            backgroundColor: '#3490db',
            alignItems: 'center',
            justifyContent: 'center',
            paddingLeft: 60,
            paddingRight: 60,
        },
        header: {
            alignSelf: 'stretch',
            fontSize: 24,
            color: '#fff',
            paddingBottom: 10,
            marginTop: 60,
            marginBottom: 40,
            borderBottomColor: '#fff',
            borderBottomWidth: 3,
        },
        textnm: {
            alignSelf: 'stretch',
            fontSize: 10,
            color: '#FFF',
            marginBottom: 30,
            paddingLeft: 1,
        },
    })










//     state = {
//         email: '',
//         password: '',
//         passwordConfirm: '',
//         error: '',
//         loading: false
//     };


//     onSingupPass = () => {

//         const { email, password } = this.state;

//         // if (this.state.password !== this.state.passwordConfirm) {
//         //     Alert.alert("Passwords do not Match");
//         //     return;
//         // }

//         this.setState({ loading: true });
//         firebase.auth().signInWithEmailAndPassword(email, password)
//             .then(() => {

//                 alert("Successful , " + email + " " + password);
//             })
//             .catch((msgError) => {

//                 alert(msgError.message);
//             });
//     }

//     renderTouchableOpacity() {
//         if (this.state.loading) {
//             return (<ActivityIndicator size='small' />);
//         } else {
//             return (<TouchableOpacity style={styles.buttonContainer}
//                 onPress={this.onSingupPass.bind(this)}>

//             </TouchableOpacity>)
//         }
//     }

//     render() {

//         return (

//             <View style={style.regis}>
//                 <Text style={style.header}>สร้างบัญชีผู้ใช้</Text>

//                 <TextInput style={style.textinput} placeholder="ชื่อ"

//                     underlineColorAndroid={'transparent'}></TextInput>


//                 <TextInput style={style.textinput} placeholder="นามสกุล"
//                     underlineColorAndroid={'transparent'}></TextInput>


//                 <TextInput
//                     placeholder="Email"
//                     placeholderTextColor="rgba(255,255,255,0.7)"
//                     returnKeyType="next"
//                     onSubmitEditing={() => this.passwordInput.focus()}
//                     keyboardType="email-address"
//                     autoCapitalize="none"
//                     autoCorrect={false}
//                     style={style.textinput}
//                     value={this.state.email}
//                     onChangeText={str => this.setState({ email: str })}
//                 />


//                 <Text style={style.textnm}>เช่น (xxx123@zzz.com)</Text>

//                 <TextInput style={style.textinput} placeholder="รหัสผ่าน"
//                     value={this.state.password}
//                     onChangeText={str => this.setState({ password: str })}
//                     secureTextEntry={true} underlineColorAndroid={'transparent'}></TextInput>

//                 <Text style={style.textnm}>ใช้อักขระและตัวเลข 6 ตัวขึ้นไป</Text>

//                 <TextInput style={style.textinput} placeholder="ยื่นยันรหัสผ่าน"
//                     value={this.state.passwordConfirm}
//                     onChangeText={str => this.setState({ passwordConfirm: str })}
//                     secureTextEntry={true} underlineColorAndroid={'transparent'}></TextInput>

//                 <View style={{  flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>


//                     <TouchableOpacity style={style.buttom1}
//                         onPress={() => this.props.navigation.navigate(Firebase)}
//                     ><Text style={style.btntext}>Back to Login</Text>
//                     </TouchableOpacity>



//                     <Button style={style.buttom2}
//                         title="Sing UP"
//                         onPress={() => this.onSingupPass.bind(this)}
//                     >
//                     </Button>
//                 </View>

//             </View>

//         );
//     }
// }


// const style = StyleSheet.create({
//     textnm: {
//         alignSelf: 'stretch',
//         color: '#FFF',
//         marginTop: -20,
//         marginBottom: 30,
//     },
//     btn: {
//         paddingLeft: 5,
//         paddingRight: 80,
//     },
//     regis: {
//         flex: 1,
//         backgroundColor: '#3490db',
//         alignItems: 'center',
//         justifyContent: 'center',
//         paddingLeft: 60,
//         paddingRight: 60,
//     },
//     header: {

//         alignSelf: 'stretch',
//         fontSize: 24,
//         color: '#fff',
//         paddingBottom: 10,
//         marginTop: 60,
//         marginBottom: 40,
//         borderBottomColor: '#fff',
//         borderBottomWidth: 3,
//     },
//     textinput: {
//         alignSelf: 'stretch',
//         backgroundColor: 'rgba(255,255,255,0.2)',
//         textShadowColor: '#BEBEBE',
//         color: '#fff',
//         marginBottom: 30,
//         paddingHorizontal: 10
//     },
//     buttom1: {
//         borderBottomColor: '#fff',
//         alignItems: 'center',
//         width: 150, height: 50,
//         padding: 15,
//         marginTop: 30,
//     },
//     buttom2: {
//         alignSelf: 'stretch',
//         alignItems: 'center',
//         padding: 20,
//         backgroundColor: '#59cbbd',
//         marginTop: 30,
//     },
//     btntext: {
//         color: '#fff',
//         fontWeight: 'bold',
//     },
//     pt: {

//         alignSelf: 'stretch',
//         alignItems: 'center',
//         justifyContent: 'center',
//         height: 200,
//         width: 200,
//         backgroundColor: '#AAA',
//     },
//     buttonContainer: {
//         backgroundColor: '#2980b9',
//         paddingVertical: 15,
//         marginBottom: 30,
//     },
// })